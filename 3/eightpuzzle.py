# each state is represented by an 9-array
# '9' represents the blank space

from random import shuffle, choice, randint

def stringify(state):
	return ''.join(str(i) for i in state)

def listify(state_string):
	return [int(i) for i in state_string]

def copy(state):
	return [x for x in state]

def legal_moves(state):
	possible_moves = []
	blank = state.index(9)
	# move right
	if blank not in [2,5,8]:
		state_copy = copy(state)
		state_copy[blank], state_copy[blank+1] = state_copy[blank+1], state_copy[blank]
		possible_moves.append(state_copy)
	# move left
	if blank not in [0,3,6]:
		state_copy = copy(state)
		state_copy[blank], state_copy[blank-1] = state_copy[blank-1], state_copy[blank]
		possible_moves.append(state_copy)
	# move up
	if blank not in [0,1,2]:
		state_copy = copy(state)
		state_copy[blank], state_copy[blank-3] = state_copy[blank-3], state_copy[blank]
		possible_moves.append(state_copy)
	# move down
	if blank not in [6,7,8]:
		state_copy = copy(state)
		state_copy[blank], state_copy[blank+3] = state_copy[blank+3], state_copy[blank] #swap
		possible_moves.append(state_copy)
	return possible_moves

def print_state(state):
	print state[0], state[1], state[2]
	print state[3], state[4], state[5]
	print state[6], state[7], state[8], "\n"

def print_states(*states):
	for state in states:
		print_state(state)

def manhattan_distance (x, y, a, b):
	return abs (x-a) + abs (y-b)

def manhattan_heuristic (state, goal):
	'''Sum of the Manhattan distances for each number'''
	result = 0
	for i in xrange(8):
		ind = state.index (i+1)
		row, col = ind / 3, ind % 3
		pos_goal = goal.index (i+1)
		row_goal, col_goal = pos_goal / 3, pos_goal % 3
		error = manhattan_distance (row, col, row_goal, col_goal)
		result += error
	return result

def displaced_heuristic (state, goal):
	'''Number of displaced tiles'''
	return len ([i for i in xrange(8) if state.index (i+1) != goal.index (i+1)])

def find_inversions(state):
	# if blank in state_copy: state_copy.remove(blank) #WTH?
	inversions = 0
	for i in state:
		counted = True
		for j in state:
			if i == j:
				counted = False
			if j < i and not counted:
				inversions += 1
	return inversions

def find_manhattan_distance(state, blank=9):
	x_dist = 2-state.index(blank)%3
	y_dist = 2-state.index(blank)/3
	return x_dist + y_dist

def find_parity(state):
	ip = 0 # even i. parity
	if find_inversions(state)%2==1:
		ip = 1
	mdp = 0 # even m.d. parity
	if find_manhattan_distance(state)%2==1:
		mdp = 1
	return (ip + mdp)%2

if __name__ == "__main__":

	# construct the graph for eight puzzle problem
	goal_state = [x+1 for x in xrange(9)]
	initial_state = copy(goal_state)
	shuffle(initial_state)
	# for i in xrange(100):
	# 	initial_state = choice(legal_moves(initial_state))
	while find_parity(initial_state) != 0:
		shuffle(initial_state)
		#print "#debug: shuffled again..."

	#initial_state = [1,2,3,4,5,6,9,7,8] #testing-only

	# print listify(initial_state), "-> (",
	# print find_inversions(initial_state), ",",
	# print find_manhattan_distance(initial_state), ",",
	# print find_parity(initial_state), ")"
	# print listify(goal_state), "-> (",
	# print find_inversions(goal_state), ",",
	# print find_manhattan_distance(goal_state), ",",
	# print find_parity(goal_state), ")"

	stack = []
	discovered = {}
	stack.append(stringify(initial_state))
	graph = {}

	weights = []
	came_from = {}

	while stack:
		current_string = stack.pop()
		discovered[current_string] = True
		graph[current_string] = set()

		for n in legal_moves(listify(current_string)):
			n_string = stringify(n)
			graph[current_string].add(n_string)
			if n_string not in discovered:
				stack.append(n_string)
				discovered[n_string] = True

	#print "Initial state:"
	#print_state(initial_state)

	# number the states
	numbering = { state : number for number, state in enumerate(graph.keys()) }

	numbers_graph = {}

	for k in graph:
		numbers_graph[numbering[k]] = [numbering[x] for x in graph[k]]

	with open ('graphs/translation-helper.txt', 'w') as f:
		for k, v in graph.items():
			f.write ("node: " + str (listify(k)) + "\n")

	#print_states(initial_state, goal_state)

	start_state_n, goal_state_n = -1, -1
	for k in graph:
		if listify(k) == initial_state:
			start_state_n = numbering[k]
		if listify(k) == goal_state:
			goal_state_n = numbering[k]

	if goal_state_n == -1:
		print "#bug: goal state never reached :("
		exit(1)

	# write to file
	with open('graphs/eight-puzzle.txt', 'w') as f:
		f.write(str(start_state_n) + " " + str(goal_state_n) + "\n")
		for k in numbers_graph:
			for v in numbers_graph[k]:
				f.write(str(v)+' ')
			f.write("\n")
		for k in numbers_graph:
			for v in numbers_graph[k]:
				f.write('1 ')
			f.write("\n")

	#print manhattan_heuristic(initial_state, goal_state)
	#print displaced_heuristic(initial_state, goal_state)
