
def init_graph(filename, graph, weights):

	with open(filename) as f:
		lines = f.readlines()

	start_state, goal_state = lines[0].split()
	start_state = int(start_state)
	goal_state = int(goal_state)

	half_lines = (len(lines) - 1) / 2
	for i in range(1, half_lines+1):
		graph.append([int(x) for x in lines[i].split()])
		weights.append([float(x) for x in lines[i+half_lines].split()])

	return start_state, goal_state

def neighbour_nodes(node, graph):

	return graph[node]

def edge_cost(node1, node2, graph, weights):

	for i,val in enumerate(graph[node1]):
		if val == node2:
			return weights[node1][i]
