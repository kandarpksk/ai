
from graphhandling import neighbour_nodes
from graphhandling import edge_cost

came_from, came_from2 = {}, {}
# Decipher funny business of
# decl/init/redecl/globals...

def bidirectional_astar(start, goal, h, graph, weights, t):
	global came_from, came_from2

	iterations, iterations2 = 0, 0
	came_from, came_from2 = {}, {}

	print "\nExecuting algorithm (bidirectional)...\n"

	start2 = goal
	goal2 = start
	h2 = h #doubtful

	closedset = set([])
	openset = set([start])
	came_from[start] = None

	g = { start : 0 }
	f = { start : g[start] + h(start, goal) }

	closedset2 = set([])
	openset2 = set([start2])
	came_from2[start2] = None

	g2 = { start2 : 0 }
	f2 = { start2 : g2[start2] + h2(start2, goal2) }

	while openset or openset2: #check logic

		current2 = -1 #check again

		if openset:

			current = min(openset, key = lambda x: f[x])
			if current in closedset2:
				further = reconstruct_path_to2(current)
				further.reverse()
				result = reconstruct_path_to(current)
				result.extend(further[1:])
				print "Iterations required:", iterations+iterations2
				print "Midway state [1]: ", t[current], "\n"
				return result

			# if current == current2:
			# 	further = reconstruct_path_to2(current2)
			# 	further.reverse()
			# 	result = reconstruct_path_to(current)
			# 	result.extend(further[1:])
			# 	print "Iterations (meet1) required:", iterations+iterations2, "\n"
			# 	return result
			# elif current == goal:
			# 	# Does this happen often/ever?
			# 	print "Iterations (start->goal) required:", iterations, "\n"
			# 	return reconstruct_path_to(goal)

			iterations += 1

			openset.remove(current)
			closedset.add(current)

			for neighbour in neighbour_nodes(current, graph):
				if neighbour in closedset:
					tentative_g = g[current] + edge_cost(current, neighbour, graph, weights)
					if tentative_g < g[neighbour]:
						print "PPR ", t[neighbour]
						came_from[neighbour] = current
						openset.add(neighbour)
						closedset.remove(neighbour)
						g[neighbour] = tentative_g
					continue

				tentative_g = g[current] + edge_cost(current, neighbour, graph, weights)

				if neighbour not in openset or tentative_g < g[neighbour]:
					came_from[neighbour] = current
					g[neighbour] = tentative_g
					f[neighbour] = g[neighbour] + h(neighbour, goal)
					if neighbour not in openset:
						openset.add(neighbour)

		if openset2:

			current2 = min(openset2, key = lambda x2: f2[x2])
			if current2 in closedset:
				further = reconstruct_path_to2(current2)
				further.reverse()
				result = reconstruct_path_to(current2)
				result.extend(further[1:])
				print "Iterations required:", iterations+iterations2
				print "Midway state [2]: ", t[current], "\n"
				return result

			# if current2 == current:
			# 	further = reconstruct_path_to2(current2)
			# 	further.reverse()
			# 	result = reconstruct_path_to(current)
			# 	result.extend(further[1:])
			# 	print "Iterations (meet2) required:", iterations2+iterations, "\n"
			# 	return result
			# elif current2 == goal2:
			# 	further = reconstruct_path_to2(goal2)
			# 	further.reverse()
			# 	print "Iterations (goal->start) required:", iterations2, "\n"
			# 	return further

			iterations2 += 1

			openset2.remove(current2)
			closedset2.add(current2)

			for neighbour2 in neighbour_nodes(current2, graph):
				if neighbour2 in closedset2:
					tentative_g2 = g2[current2] + edge_cost(current2, neighbour2, graph, weights)
					if tentative_g2 < g2[neighbour2]:
						print "PPR ", t[neighbour2]
						came_from2[neighbour2] = current2
						openset2.add(neighbour2)
						closedset2.remove(neighbour2)
						g2[neighbour2] = tentative_g2
					continue

				tentative_g2 = g2[current2] + edge_cost(current2, neighbour2, graph, weights)

				if neighbour2 not in openset2 or tentative_g2 < g2[neighbour2]:
					came_from2[neighbour2] = current2
					g2[neighbour2] = tentative_g2
					f2[neighbour2] = g2[neighbour2] + h2(neighbour2, goal2)
					if neighbour2 not in openset2:
						openset2.add(neighbour2)

	return 'failed'

def reconstruct_path_to(current_node):

	if current_node in came_from:
		p = reconstruct_path_to(came_from[current_node])
		p.append(current_node)
		return p
	elif current_node:
		return [current_node]
	else: return []

def astar_twice(start, goal, h, graph, weights, t):
	global came_from, came_from2

	iterations, iterations2 = 0, 0
	came_from, came_from2 = {}, {}

	print "Executing algorithms (separately)...\n"

	start2 = goal
	goal2 = start
	h2 = h #doubtful

	closedset = set([])
	openset = set([start])
	came_from[start] = None

	g = { start : 0 }
	f = { start : g[start] + h(start, goal) }

	closedset2 = set([])
	openset2 = set([start2])
	came_from2[start2] = None

	g2 = { start2 : 0 }
	f2 = { start2 : g2[start2] + h2(start2, goal2) }

	while openset or openset2: #check logic

		current2 = -1 #check again

		if openset:

			current = min(openset, key = lambda x: f[x])
			if current == goal:
				print "Iterations (start->goal) required:", iterations
				return reconstruct_path_to(goal)

			iterations += 1

			openset.remove(current)
			closedset.add(current)

			for neighbour in neighbour_nodes(current, graph):
				if neighbour in closedset:
					tentative_g = g[current] + edge_cost(current, neighbour, graph, weights)
					if tentative_g < g[neighbour]:
						print "PPR ", t[neighbour]
						came_from[neighbour] = current
						openset.add(neighbour)
						closedset.remove(neighbour)
						g[neighbour] = tentative_g
					continue

				tentative_g = g[current] + edge_cost(current, neighbour, graph, weights)

				if neighbour not in openset or tentative_g < g[neighbour]:
					came_from[neighbour] = current
					g[neighbour] = tentative_g
					f[neighbour] = g[neighbour] + h(neighbour, goal)
					if neighbour not in openset:
						openset.add(neighbour)

		if openset2:

			current2 = min(openset2, key = lambda x2: f2[x2])
			if current2 == goal2:
				further = reconstruct_path_to2(goal2)
				further.reverse()
				print "Iterations (goal->start) required:", iterations2
				return further

			iterations2 += 1

			openset2.remove(current2)
			closedset2.add(current2)

			for neighbour2 in neighbour_nodes(current2, graph):
				if neighbour2 in closedset2:
					tentative_g2 = g2[current2] + edge_cost(current2, neighbour2, graph, weights)
					if tentative_g2 < g2[neighbour2]:
						print "PPR ", t[neighbour2]
						came_from2[neighbour2] = current2
						openset2.add(neighbour2)
						closedset2.remove(neighbour2)
						g2[neighbour2] = tentative_g2
					continue

				tentative_g2 = g2[current2] + edge_cost(current2, neighbour2, graph, weights)

				if neighbour2 not in openset2 or tentative_g2 < g2[neighbour2]:
					came_from2[neighbour2] = current2
					g2[neighbour2] = tentative_g2
					f2[neighbour2] = g2[neighbour2] + h2(neighbour2, goal2)
					if neighbour2 not in openset2:
						openset2.add(neighbour2)

	return 'failed'

def reconstruct_path_to2(current_node2):

	if current_node2 in came_from2:
		p2 = reconstruct_path_to2(came_from2[current_node2])
		p2.append(current_node2)
		return p2
	elif current_node2:
		return [current_node2]
	else: return []
