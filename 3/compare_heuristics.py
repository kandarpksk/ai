import numpy as np
from matplotlib import pyplot as plt

plt.title('Comparison of different heuristics for eight puzzle')

y1, y2 = [], []
with open('eight-puzzle-data.txt') as f:
	for line in f:
		i1, i2 = [int(x) for x in line.split()]
		y1.append(i1)
		y2.append(i2)

width = .35
iterations = [100, 10]

plt.xlabel('Heuristic')
plt.ylabel('Iterations to reach goal state')

plt.plot(y1)
plt.plot(y2)
plt.savefig('comparison.png')
plt.close()