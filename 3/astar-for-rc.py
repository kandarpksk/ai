import re
import sys

graph = []
weights = []
came_from = {}

def init_graph(filename):

	with open(filename) as f:
		lines = f.readlines()

	start_state, goal_state = lines[0].split()
	start_state = int(start_state)
	goal_state = int(goal_state)

	half_lines = (len(lines) - 1) / 2
	for i in range(1, half_lines+1):
		graph.append([int(x) for x in lines[i].split()])
		weights.append([float(x) for x in lines[i+half_lines].split()])

	return start_state, goal_state

start_state, goal_state = init_graph('graphs/river-crossing.txt')

# print graph

iterations = 0
def astar(start, goal, h):
	global iterations # Python Weirdness!

	closedset = set()
	openset = set([start])
	came_from[start] = None

	g = { start : 0 }
	f = { start : g[start] + h(start, goal) }
	print "\tupdated f[" + str(start) + "]\t\titr", iterations

	while openset:

		current = min(openset, key = lambda x: f[x])
		if current == goal:
			return reconstruct_path_to(goal)

		iterations = iterations + 1

		openset.remove(current)
		closedset.add(current)

		for neighbour in neighbour_nodes(current):
			if neighbour in closedset:
				continue

			tentative_g = g[current] + edge_cost(current, neighbour)

			if neighbour not in openset or tentative_g < g[neighbour]:
				if neighbour in came_from:
					print "Redirected parent pointer of", neighbour, "to", current
					
				came_from[neighbour] = current
				g[neighbour] = tentative_g

				f[neighbour] = g[neighbour] + h(neighbour, goal)
				print "\tupdated f[", neighbour, "]\t\titr", iterations
				if neighbour not in openset:
					openset.add(neighbour)

	return 'failed'

def reconstruct_path_to(current_node):

	if current_node in came_from:
		p = reconstruct_path_to(came_from[current_node])
		p.append(current_node)
		return p
	else:
		return [current_node]

def neighbour_nodes(node):

	return graph[node]

def edge_cost(node1, node2):

	for i,val in enumerate(graph[node1]):
		if val == node2:
			return weights[node1][i]

p = re.compile ('\d+')
t = [] #translation (inevitable)
with open ('graphs/translation-helper.txt') as f:
	lines = f.readlines()
for i in range (0, len (lines) ):
	t.append (p.findall (lines[i]) )

def bfs_heuristic(node, goal_node):
	return 0

# heuristic
def h(node, goal_node):
	return bfs_heuristic(node, goal_node)
	# res = 0
	# if int(t[node][2]) == 0: # state of left bank
	# 	#res = int(t[node][0]) + int(t[node][1])*5 		# 2/3: 6,15	-- 4: 6,13 -- 5: 6,12 -- 6: 6,15
	# 	res = int(t[node][0])*3 + int(t[node][1]) 		# 2: 5,16	-- 3: 5,17 -- 4: 5,18
	# 	#res = abs(int(t[node][0]) - int(t[node][1])) 	# 7,37  | Reduce all by 1 to make up
	# 	#res = 1 										# 10,78 | for a counting oversight...
	# else: # state of right bank
	# 	#res = int(t[node][0])*5 + int(t[node][1]) # Reversed
	# 	res = int(t[node][0]) + int(t[node][1])*3 # Reversed
	# 	#res = abs(int(t[node][0]) - int(t[node][1]))
	# 	#res = 1
	# print t[node], "\t", res,
	# return res

s = astar (start_state, goal_state, h) [1:] #solution
#print #sys.stdout.write
print "Iterations required:", iterations, "\n"

if s != "ailed":
	for i in range (0, len (s) ):
		if i == 0:
			print t[s[i]], "Initial State"
		elif i == len(s)-1:
			print t[s[i]], "Final State"
		elif i%2 == 1:
			print t[s[i]], int(t[s[i-1]][0])-int(t[s[i]][0]), "m to right bank,", int(t[s[i-1]][1])-int(t[s[i]][1]), "c to right bank"
		else:
			print t[s[i]], int(t[s[i]][0])-int(t[s[i-1]][0]), "m to left bank, ", int(t[s[i]][1])-int(t[s[i-1]][1]), "c to left bank"
else:
	print "No solution"