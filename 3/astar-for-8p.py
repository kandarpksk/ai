
import re, sys

from eightpuzzle import *
from bidirectional import *
from graphhandling import init_graph

graph = []
weights = []

print "Reading input graph..."
start_state, goal_state = init_graph('graphs/eight-puzzle.txt', graph, weights)
start_state2, goal_state2 = goal_state, start_state

print "Reading node translation..."
p = re.compile ('\d+')
t = [] # translation
with open ('graphs/translation-helper.txt') as f:
	lines = f.readlines()
for i in range (0, len (lines) ):
	t.append (map (int, p.findall (lines[i])))

# heuristic
def h(node, goal_node):
	return 1

def unbiased_h(node, goal_node):
	return 0 # Do not use!

def manhattan_h(node, goal_node):
	t_node = [int(x) for x in t[node]]
	t_goal_node = [int(x) for x in t[goal_node]]
	return manhattan_heuristic(t_node, t_goal_node)

def displaced_h(node, goal_node):
	t_node = [int(x) for x in t[node]]
	t_goal_node = [int(x) for x in t[goal_node]]
	return displaced_heuristic(t_node, t_goal_node)

print_steps = True
def print_board (x):
	print t[x][:3]
	print t[x][3:6]
	print t[x][6:]

if __name__ == "__main__":
	s, s2 = '', '' # solution(s)
	if sys.argv[1] == 'displaced':
		# Why is start state repeated??
		s = bidirectional_astar (start_state, goal_state, displaced_h, graph, weights, t) [1:]
		s2 = astar_twice (start_state, goal_state, displaced_h, graph, weights, t) [1:]
		if (s != s2): print "*** Unsurprisingly, didn't find optimal path! ***"
		print "" # left empty
	elif sys.argv[1] == 'manhattan':
		s = bidirectional_astar (start_state, goal_state, manhattan_h, graph, weights, t) [1:]
		s2 = astar_twice (start_state, goal_state, manhattan_h, graph, weights, t) [1:]
		if (s != s2): print "*** Unsurprisingly, didn't find optimal path! ***"
		print "" # left empty
	else:
		print "\nAlgorithm incorrectly/not specified :("
		exit(0)

	if s2 != "ailed":
		if print_steps:
			for i in range (0, len (s)): # What about printing both!?
				if i == 0:
					print "Initial State"
					print_board (s2[i]);
					print
				elif i == len(s2)-1:
					print_board (s2[i]);
					print "Final State"
				else:
					print_board (s2[i]);
					print
		else:
			print "Initial state: ", t[s2[0]]
			print "Final state:", t[s2[len(s2)-1]]
	else:
		print "No solution"
