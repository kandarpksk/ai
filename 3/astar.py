from random import shuffle, choice, randint

def stringify(state):
	return ''.join(str(i) for i in state)

def listify(state_string):
	return [int(i) for i in state_string]

def copy(state):
	return [x for x in state]

def legal_moves(state):
	possible_moves = []
	blank = state.index(9)
	# move right
	if blank not in [2,5,8]:
		state_copy = copy(state)
		state_copy[blank], state_copy[blank+1] = state_copy[blank+1], state_copy[blank]
		possible_moves.append(state_copy)
	# move left
	if blank not in [0,3,6]:
		state_copy = copy(state)
		state_copy[blank], state_copy[blank-1] = state_copy[blank-1], state_copy[blank]
		possible_moves.append(state_copy)
	# move up
	if blank not in [0,1,2]:
		state_copy = copy(state)
		state_copy[blank], state_copy[blank-3] = state_copy[blank-3], state_copy[blank]
		possible_moves.append(state_copy)
	# move down
	if blank not in [6,7,8]:
		state_copy = copy(state)
		state_copy[blank], state_copy[blank+3] = state_copy[blank+3], state_copy[blank] #swap
		possible_moves.append(state_copy)
	return possible_moves

start = [1,2,3,4,5,6,7,8,9]
goal = [1,2,3,4,5,6,7,8,9]

openlist = set([start])
closedlist = set()

def astar(start,goal,heuristic):
	parent = {start:None}
	while openlist:
		current = min(openlist, key = lambda x: f[x])
		openlist.remove(current)
		closedlist.add(current)
		if current == goal:
			return reconstruct_path_to(goal)
		for neighbour in legal_moves(state):
			if parent[current] != 

	return 'failed'


