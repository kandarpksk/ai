### merely concatenated

from sys import argv
import re

# m = int(input('Number of missionaries: '))
# c = int(input('Number of cannibals: '))
# b = int(input('Capacity of boat: '))
m = 3
c = 3
b = 2
# general case, num_m need not equal num_c (solvability not guaranteed)

# num_states = (m+1)*(c+1)*2 # not all states can be reached!

stack = []
discovered = {}
initial_state = (m,c,0) # b = 0 for left, and 1 for right
stack.append(initial_state)
graph = {}

# each state is a tuple (missionaries_on_left_bank, cannibals_on_left_bank, is_boat_on_left_bank)

while stack:
	current = stack.pop()
	discovered[current] = True
	mm, cc, bb = current

	neighbours = []
	if bb == 1:
		for i in xrange(0,b+1):
			for j in xrange(0,b+1-i):
				if (i or j) and (mm+i <= m and cc+j <= c):
					neighbours.append((mm+i,cc+j,0))
	else:
		for i in xrange(0,b+1):
			for j in xrange(0,b+1-i):
				if (i or j)  and (mm-i >= 0 and cc-j >= 0):
					neighbours.append((mm-i,cc-j,1))

	graph[current] = set([])

	# print current, neighbours

	for n in neighbours:
		# check if n is valid i.e.
		# for each bank, either m = 0 or m > 0 and m > c
		if (n[0] == 0 or n[0] >= n[1]) and (n[0] == m or n[0] <= n[1]):
			graph[current].add(n)
			if n not in discovered:
				stack.append(n)
				discovered[n] = True

# number the states
numbering = {state:number for number,state in enumerate(graph.keys())}

numbers_graph = {}

for k in graph:
	numbers_graph[numbering[k]] = [numbering[x] for x in graph[k]]

##for k,v in graph.items():
	##print 'Node', k, 'has neighbours:', v, 'with numbering', numbering[k]

##with open ('graphs/translation-helper.txt', 'w') as f:
	##for k in graph.items():
		##f.write ("node: " + str (k) + "\n")

# for k,v in numbers_graph.items():
	# print 'Node', k, 'has neighbours:', v

# find start state and goal state
start_state, goal_state = -1, -1
for k in graph:
	mm, cc, bb = k
	if mm == m and cc == c and bb == 0:
		start_state = numbering[k]
	if mm == 0 and cc == 0:
		goal_state = numbering[k]

# write to file
with open('graphs/river-crossing.txt', 'w') as f:
	f.write(str(start_state) + " " + str(goal_state) + "\n")
	for k in numbers_graph:
		for v in numbers_graph[k]:
			f.write(str(v)+' ')
		f.write("\n")
	for k in numbers_graph:
		for v in numbers_graph[k]:
			f.write('1 ')
		f.write("\n")

###

graph = []
weights = []
came_from = {}

def init_graph(filename):
	global graph, weights

	with open(filename) as f:
		lines = f.readlines()

	start_state, goal_state = lines[0].split()
	start_state = int(start_state)
	goal_state = int(goal_state)

	half_lines = (len(lines) - 1) / 2
	for i in range(1, half_lines+1):
		graph.append([int(x) for x in lines[i].split()])
		weights.append([float(x) for x in lines[i+half_lines].split()])

	return start_state, goal_state

if len(argv) == 1:
	print 'usage: python astar.py graph-file-path'
	exit(0)

start_state, goal_state = init_graph(argv[1])

def astar(start, goal, h):
	global graph, weights, came_from

	closedset = set([])
	openset = set([start])
	came_from[start] = None

	g = { start : 0 }
	f = { start : g[start] + h(start, goal) }

	while openset:
		current = min(openset, key = lambda x: f[x])
		if current == goal:
			return reconstruct_path_to(goal)

		openset.remove(current)
		closedset.add(current)

		for neighbour in neighbour_nodes(current):
			if neighbour in closedset:
				continue
			tentative_g = g[current] + edge_cost(current, neighbour)

			if neighbour not in openset or tentative_g < g[neighbour]:
				came_from[neighbour] = current
				g[neighbour] = tentative_g
				f[neighbour] = g[neighbour] + h(neighbour, goal)
				if neighbour not in openset:
					openset.add(neighbour)

	return 'failed'

def reconstruct_path_to(current_node):
	global came_from
	if current_node in came_from:
		p = reconstruct_path_to(came_from[current_node])
		p.append(current_node)
		return p
	else:
		return [current_node]

def neighbour_nodes(node):
	global graph
	return graph[node]

# heuristic
def h(node, goal_node):
	global graph, weights
	return 1

def edge_cost(node1, node2):
	global weights, graph
	for i,val in enumerate(graph[node1]):
		if val == node2:
			return weights[node1][i]

###

p = re.compile ('\d+')
t = [] #translation (inevitable)
with open ('graphs/translation-helper.txt') as f:
	lines = f.readlines()
for i in range (0, len (lines) ):
	t.append (p.findall (lines[i]) )

s = astar (start_state,goal_state,h) [1:] #solution
for i in range (0, len (s) ):
	if i == 0: print t[s[i]], "Initial State"
	elif i == len(s)-1: print t[s[i]], "Final State"
	elif i%2 == 1: print t[s[i]], int(t[s[i-1]][0])-int(t[s[i]][0]), "m go/es, ", int(t[s[i-1]][1])-int(t[s[i]][1]), "c go/es"
	else: print t[s[i]], int(t[s[i]][0])-int(t[s[i-1]][0]), "m come/s,", int(t[s[i]][1])-int(t[s[i-1]][1]), "c come/s [back]"
