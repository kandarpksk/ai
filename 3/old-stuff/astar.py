from sys import argv

graph = []
weights = []
came_from = {}

def init_graph(filename):
	global graph, weights

	with open(filename) as f:
		lines = f.readlines()

	start_state, goal_state = lines[0].split()
	start_state = int(start_state)
	goal_state = int(goal_state)

	half_lines = (len(lines) - 1) / 2
	for i in range(1, half_lines+1):
		graph.append([int(x) for x in lines[i].split()])
		weights.append([float(x) for x in lines[i+half_lines].split()])

	return start_state, goal_state

if len(argv) == 1:
	print 'usage: python astar.py graph-file-path'
	exit(0)

start_state, goal_state = init_graph(argv[1])

# print graph

def astar(start, goal, h):
	global graph, weights, came_from

	closedset = set([])
	openset = set([start])
	came_from[start] = None

	g = { start : 0 }
	f = { start : g[start] + h(start, goal) }

	while openset:
		current = min(openset, key = lambda x: f[x])
		if current == goal:
			return reconstruct_path_to(goal)

		openset.remove(current)
		closedset.add(current)

		for neighbour in neighbour_nodes(current):
			if neighbour in closedset:
				continue
			tentative_g = g[current] + edge_cost(current, neighbour)

			if neighbour not in openset or tentative_g < g[neighbour]:
				came_from[neighbour] = current
				g[neighbour] = tentative_g
				f[neighbour] = g[neighbour] + h(neighbour, goal)
				if neighbour not in openset:
					openset.add(neighbour)

	return 'failed'

def reconstruct_path_to(current_node):
	global came_from
	if current_node in came_from:
		p = reconstruct_path_to(came_from[current_node])
		p.append(current_node)
		return p
	else:
		return [current_node]

def neighbour_nodes(node):
	global graph
	return graph[node]

# heuristic
def h(node, goal_node):
	global graph, weights
	return 1

def edge_cost(node1, node2):
	global weights, graph
	for i,val in enumerate(graph[node1]):
		if val == node2:
			return weights[node1][i]

print astar(start_state,goal_state,h)[1:]
