from sys import argv

# this file generates the file './graphs/river-crossing.txt'
#

# m = int(input('Number of missionaries: '))
# c = int(input('Number of cannibals: '))
# b = int(input('Capacity of boat: '))
m = int (argv[1]) ##3
c = int (argv[2]) ##3
b = int (argv[3]) ##2
# general case, num_m need not equal num_c (solvability not guaranteed)

# num_states = (m+1)*(c+1)*2 # not all states can be reached!

stack = []
discovered = {}
initial_state = (m,c,0) # b = 0 for left, and 1 for right
stack.append(initial_state)
graph = {}

# each state is a tuple (missionaries_on_left_bank, cannibals_on_left_bank, is_boat_on_left_bank)

while stack:
	current = stack.pop()
	discovered[current] = True
	mm, cc, bb = current

	neighbours = []
	if bb == 1:
		for i in xrange(0,b+1):
			for j in xrange(0,b+1-i):
				if (i or j) and (mm+i <= m and cc+j <= c):
					neighbours.append((mm+i,cc+j,0))
	else:
		for i in xrange(0,b+1):
			for j in xrange(0,b+1-i):
				if (i or j)  and (mm-i >= 0 and cc-j >= 0):
					neighbours.append((mm-i,cc-j,1))

	graph[current] = set([])

	# print current, neighbours

	for n in neighbours:
		# check if n is valid i.e.
		# for each bank, either m = 0 or m > 0 and m > c
		if (n[0] == 0 or n[0] >= n[1]) and (n[0] == m or n[0] <= n[1]):
			graph[current].add(n)
			if n not in discovered:
				stack.append(n)
				discovered[n] = True

# number the states
numbering = {state:number for number,state in enumerate(graph.keys())}

numbers_graph = {}

for k in graph:
	numbers_graph[numbering[k]] = [numbering[x] for x in graph[k]]

#####for k,v in graph.items():
	#####print 'Node', k, 'has neighbours:', v, 'with numbering', numbering[k]

with open ('graphs/translation-helper.txt', 'w') as f:
	for k, v in graph.items():
		f.write ("node: " + str (k) + "\n")

# for k,v in numbers_graph.items():
	# print 'Node', k, 'has neighbours:', v

# find start state and goal state
start_state, goal_state = -1, -1
for k in graph:
	mm, cc, bb = k
	if mm == m and cc == c and bb == 0:
		start_state = numbering[k]
	if mm == 0 and cc == 0:
		goal_state = numbering[k]

# write to file
with open('graphs/river-crossing.txt', 'w') as f:
	f.write(str(start_state) + " " + str(goal_state) + "\n")
	for k in numbers_graph:
		for v in numbers_graph[k]:
			f.write(str(v)+' ')
		f.write("\n")
	for k in numbers_graph:
		for v in numbers_graph[k]:
			f.write('1 ')
		f.write("\n")
