River Crossing Problem
======================

Eight Puzzle Problem
====================

To run the eight puzzle problem, run the bash script run-8p.sh.

It runs both the Manhattan and Displaced Tiles heuristics and outputs the number of iterations for both of them. We ran the script a few times and copied its output into a file named 'eight-puzzle-data.txt'. Then we plotted it using Matplotlib. The plotting code can be found in 'compare-heuristics.py'.

To run the plotting script, do 'python compare-heuristics.py'. The output is a PNG file named 'comparison.png'. It clearly shows that the Manhattan heuristic is much faster (fewer iterations needed) than the Displaced Tiles heuristic.

To see where parent pointer redirection is happening, comment out the block of code in the 