#!/bin/bash

if [ $# -eq 3 ]; then
	mkdir -p graphs
	rm -f graphs/eight-puzzle.txt
	pypy river-crossing.py $1 $2 $3
	pypy astar-for-rc.py
else
	echo
	echo "Run as ./run-rc.sh #missionaries #cannibals boat-capacity"
	echo
fi
