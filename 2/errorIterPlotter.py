import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
#lrates vary as 0.1,0.2,0.3,0.5,0.7,0.9
data=np.genfromtxt('./results.txt', delimiter=',', skip_header=1, names=['x','y'])
data1=np.genfromtxt('./results1.txt', delimiter=',', skip_header=1, names=['x','y'])
data2=np.genfromtxt('./results2.txt', delimiter=',', skip_header=1, names=['x','y'])
data3=np.genfromtxt('./results3.txt', delimiter=',', skip_header=1, names=['x','y'])
data4=np.genfromtxt('./results4.txt', delimiter=',', skip_header=1, names=['x','y'])
data5=np.genfromtxt('./results5.txt', delimiter=',', skip_header=1, names=['x','y'])
fig =plt.figure()
ax1 = fig.add_subplot(111)

ax1.set_title( "Error v/s Iterations" )
ax1.set_xlabel( "Iterations" )
ax1.set_ylabel( "Error" )

ax1.plot(data['x'],data['y'],c='b',linestyle='-',marker='o')
ax1.plot(data1['x'],data1['y'],c='g',linestyle='-',marker='o')
ax1.plot(data2['x'],data2['y'],c='r',linestyle='-',marker='o')
ax1.plot(data3['x'],data3['y'],c='black',linestyle='-',marker='o')
ax1.plot(data4['x'],data4['y'],c='cyan',linestyle='-',marker='o')
ax1.plot(data5['x'],data5['y'],c='brown',linestyle='-',marker='o')
leg=ax1.legend()
plt.show()