digits=[
	[[1, 1, 1, 0, 1, 1, 1],0],
	[[0, 0, 1, 0, 0, 1, 0],1],
	[[1, 0, 1, 1, 1, 0, 1],2],
	[[1, 0, 1, 1, 0, 1, 1],3],
	[[0, 1, 1, 1, 0, 1, 0],4],
	[[1, 1, 0, 1, 0, 1, 1],5],
	[[1, 1, 0, 1, 1, 1, 1],6],
	[[1, 0, 1, 0, 0, 1, 0],7],
	[[1, 1, 1, 1, 1, 1, 1],8],
	[[1, 1, 1, 1, 0, 1, 1],9]
]

p "Enter light vector"

lightVector=gets.gsub(/\s+/,'')[1...-1].split(',').map(&:to_i)

d=digits.delete_if{|x| x[0]!=lightVector}
if d==[]
	p "Doesn't match anything"
else
	print "The lightVector is the digit : "
	p d[0][1]
end