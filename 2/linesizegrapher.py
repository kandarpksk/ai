import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook

data=np.genfromtxt('./results.txt', delimiter=',', skip_header=1, names=['x','y'])

fig =plt.figure()
ax1 = fig.add_subplot(111)

ax1.set_title( "Error v/s Iterations" )
ax1.set_xlabel( "Iterations" )
ax1.set_ylabel( "Error" )

ax1.plot(data['x'],data['y'],c='b',linestyle='-',marker='o')
leg=ax1.legend()
plt.show()