ssdFLAG=false
def sigmoid(n); e = Math::E; 1.0/(1+e**(-n)); end
class Float; def activate(t); self > t ? 1 : 0; end; end

class Node
	attr_accessor :recentOutput,:recentInput,:error,:outEdges,:inEdges
	def initialize
		@recentOutput, @recentInput, @error = nil, nil, nil
		@outEdges, @inEdges = [], []
	end

	def eval(inputVector)
		return @recentOutput unless @recentOutput.nil?
		@recentInput = []
		dotprod = 0
		@inEdges.each do |e|
			ip = e.src.eval(inputVector)
			@recentInput << ip
			dotprod += e.weight * ip
		end
		@recentOutput = sigmoid(dotprod)
		return @recentOutput
	end
	#done only for output nodes
	def errorCalc(expected)
		return @error unless @error.nil?

		if @outEdges == [] then @error = expected - @recentOutput
		else @error = @outEdges.map { |e| e.weight*e.dest.errorCalc(expected) }.inject(:+)
		end
		return @error
	end

	def updateWeights(lrate)
		if !@error.nil? and !@recentOutput.nil? and !@recentInput.nil?
			@inEdges.each_with_index { |e,i| e.weight += lrate*@recentOutput*(1-@recentOutput)*@error*@recentInput[i] }
			@outEdges.each { |e| e.dest.updateWeights(lrate) }
			@error, @recentInput, @recentOutput = nil, nil, nil
		end
	end

	def remComputations
		unless @recentOutput.nil?
			@recentOutput = nil
			@inEdges.each { |e| e.src.remComputations }
		end
	end
end

class InputNode < Node
	attr_accessor :recentOutput,:recentInput,:error,:outEdges,:inEdges,:index
	def initialize(index)
		super()
		@index = index
	end

	def eval(inputVector)
		@recentOutput = inputVector[@index]
		return @recentOutput
	end

	def updateWeights(lrate)
		@outEdges.each { |e| e.dest.updateWeights(lrate) }
	end

	def errorCalc(expected)
		@outEdges.each { |e| e.dest.errorCalc(expected) }
	end
		
	def remComputations
		@recentOutput = nil
	end
end

class BiasNode < InputNode
	def initialize
		grandparent = self.class.superclass.superclass
		meth = grandparent.instance_method(:initialize)
		meth.bind(self).call
	end

	def eval(inputVector)
		return 1
	end
end

class Edge
	attr_accessor :weight,:src,:dest
	def initialize(src, dest)
		@weight, @src, @dest = rand, src, dest
		@src.outEdges << self
		@dest.inEdges << self
	end
end

class NeuralNet
	attr_accessor :inputNodes,:outputNode
	def initialize
		@inputNodes = []
		@outputNode = nil
	end

	def eval(inputVector)
		@outputNode.remComputations
		return @outputNode.eval(inputVector)
	end

	def propogateError(expected)
		@inputNodes.each{ |n| n.errorCalc(expected) }
	end

	def updateWeights(lrate)
		@inputNodes.each{ |n| n.updateWeights(lrate) }
	end

	def trainItAway(truthtable, lrate, numIter)
		while numIter > 0
			truthtable.each do |entry|
				op = eval(entry[0])
				propogateError(entry[1])
				updateWeights(lrate)
				numIter -= 1
			end			
		end
	end
end

#######Done defining the Neural Network
######p "Give number of Bits:"
n=gets.to_i
neuralNetwork=NeuralNet.new
inputNodes=(1..n).map{|i| InputNode.new(i-1)}
hiddenNodes = (1..n).map{|i| Node.new}
outputNode=Node.new
hiddenNodes.each{|h| h.inEdges << Edge.new(BiasNode.new,h)}

Edge.new(BiasNode.new(),outputNode)

inputNodes.each do |i|
	hiddenNodes.each do |j|
		Edge.new(i,j)	
	end
end

hiddenNodes.each { |j| Edge.new(j,outputNode) }

neuralNetwork.outputNode = outputNode
neuralNetwork.inputNodes += inputNodes

##################p "Enter the truth table outputs"
if !ssdFLAG
nrows = (1 << n)
inputs = (0...nrows).map { |i| i.to_s(2).rjust(n,'0').split('').map(&:to_i) }
#outputs = (0...nrows).map { gets.to_i }
########Use this one for parity function########
outputs=inputs.map{|x| x.inject(:^)}
p outputs
#p "Input taken in completely!"
truthtable = inputs.zip(outputs)
#p "training started"
neuralNetwork.trainItAway(truthtable,0.999999999999,10000000)

inputs.each do |row|
	#print row.inspect, " "
	#puts neuralNetwork.eval(row)
end

nnOutputs=inputs.map{|row| neuralNetwork.eval(row).activate(0.5)}
#p "nnOutputs"

p nnOutputs

nnIP=inputs.zip(nnOutputs)
#p nnIP
#p neuralNetwork.eval([1, 1, 1, 1, 1, 1, 1])
else
	nrows=(1<<7)
	inputs = (0...nrows).map { |i| i.to_s(2).rjust(n,'0').split('').map(&:to_i) }
	outputs = (0...nrows).map { gets.to_i }
	tt=inputs.zip(outputs)
	neuralNetwork.trainItAway(tt,0.999999999999,100000)
	nnOutputs=inputs.delete_if{|row| neuralNetwork.eval(row).activate(0.5)==0}
	p nnOutputs[0]
end




