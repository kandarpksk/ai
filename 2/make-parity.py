# usage: pypy make-parity.py 5

import sys

number = sys.argv[1]

with open('training-data/' + number + '-input-parity.txt', 'w') as f:
	f.write(number + "\n")
	for i in xrange(1 << int(number)):
		xor = lambda x,y: x ^ y
		output = reduce(xor, [int(x) for x in "{0:05b}".format(i)])
		f.write(str(output) + "\n")