# run this like so:
# ruby ffnn.rb < training-data/[filename]

def rand_arr(n); (0...n).map { rand }; end
def sigmoid(n); e = Math::E; 1.0/(1+e**(-n)); end
def dotprd(a1,a2); a1.zip(a2).map{ |x,y| x*y }.reduce(:+); end
class Float; def activate(t); self > t ? 1 : 0; end; end

lrate, err_change, nrows = gets.to_f, 0, (1 << (n = gets.to_i))
m, expected = gets.to_i, (0...nrows).map { gets.to_i }

weights1, weights0 = rand_arr(m), (0...n).map { rand_arr(m) } # weights
deltaw1, deltaw0 = [0]*m, [[0]*m]*n # weight changes

# outsi = outputs of the layer i neurons
outs0 = (0...nrows).map { |i| i.to_s(2).rjust(n,'0').split('').map(&:to_i) }
outs1, outs2 = [[0]*m]*nrows, [0]*nrows

# deltasi = changes in weights from row i to row i+1
deltas2, deltas1, deltas0 = [0]*nrows, [[0]*m]*nrows, [[0]*n]*nrows

iterations, err = 0, 1

loop do
  nrows.times do |r|
    ## forward propagate the outputs. first, layer 0 to 1, then, layer 1 to layer 2
    m.times { |j| outs1[r][j] = sigmoid(dotprd(weights0.transpose[j], outs0[r])) }
    outs2[r] = sigmoid(dotprd(weights1, outs1[r]))

    ## backpropagate the errors. find deltas for layer 2, then layer 1, then layer 0
    tj, oj = expected[r], outs2[r]; deltas2[r] = (tj - oj) * oj * (1 - oj)
    m.times { |j| deltas1[r][j] = weights1[j] * deltas2[r] }
    n.times { |i| deltas0[r][i] = dotprd(weights0[i], deltas1[r]) }

    ## find the weight changes. first, layer 1 to 2, then, layer 0 to 1
    m.times { |j| deltaw1[j] = lrate * deltas2[r] * outs1[r][j] }
    n.times { |i| m.times { |j| deltaw0[i][j] = lrate * deltas1[r][j] * outs0[r][i] } }

    ## having found the weight changes, actually change the weights
    m.times { |j| weights1[j] += deltaw1[j] }
    n.times { |i| m.times { |j| weights0[i][j] += deltaw0[i][j] } }
  end

  # calculate the net error over all input patterns and then the change in error
  err_change = (err_new = 0.5 * expected.zip(outs2).map { |x,y| (x-y)**2 }.reduce(:+)) - err

  if err_change > 0 || err_change.abs < 1e-9
    puts "Iteration #{iterations} , Error change #{err_change}"
    break
  end

  puts "Iteration #{iterations} , Error change #{err_change}" if iterations % 20000 == 0
  err, iterations = err_new, iterations+1
end

p outs2
p outs2.map { |out| out.activate(0.4) }