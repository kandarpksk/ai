#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
vector<string> answer;

vector<int> s2Vec(string s){
	vector<int> v;
	v.push_back(-1);
	for(int i=0;i<s.length();i++){
		if(s[i]=='1')
			v.push_back(1);
		else
			v.push_back(0);

	}
	return v;
}
void getStrings( string s, int digitsLeft )
{
   if( digitsLeft == 0 ) // the length of string is n
      answer.push_back( s );
   else
   {
      getStrings( s + "0", digitsLeft - 1 );
      getStrings( s + "1", digitsLeft - 1 );
   }
}

bool isPalindrome(string s){
	bool isPalin=true;
	for(int i=0;i<s.length();i++){
		isPalin=isPalin && (s[i]==s[s.length()-i-1]);

	}
	return isPalin;
}

bool majority(string s){
	int ans=0;
	for(int i=0;i<s.length();i++){
		if(s[i]=='1')
			ans++;
	}
	return (ans > (s.length() - ans));

}

bool evenParity(string s){
	int ans=0;
	for(int i=0;i<s.length();i++){
		if(s[i]=='1')
			ans++;
	}
	return (ans%2==0);

}

int main(){
	
	int n=2;
	getStrings( "", n );
	
	cout<<"2 input NAND"<<endl;
	//a) 2 input NAND
	for(string s:answer){
		vector<int> v=s2Vec(s);
		int clas=!(v[1]&v[2]);
		
		cout<< clas<<endl;
//cout<<clas<<endl;

		if(clas==0){
			for(int i=0;i<n;i++){
				v[i]=-v[i];
			}

		}
		

	}

	//a) 2 input OR
	cout<<"2 input OR"<<endl;
	for(string s: answer){
		vector<int> v=s2Vec(s);
		int clas=(v[1]|v[2]);
		
		cout<< clas<<endl;

	}
	//b) 2 input XOR
	cout<<"2 input XOR"<<endl;
	for(string s: answer){
		vector<int> v=s2Vec(s);
		int clas=(v[1]^v[2]);
		
		cout<< clas<<endl;

	}
	//c) 5 input palindrome
	cout<< "5 input palindrome"<<endl;
	n=5;
	while(!answer.empty()){
		answer.pop_back();
	}
	getStrings("",n);
	for(string s: answer){
		vector<int> v=s2Vec(s);
		int clas=(isPalindrome(s));
		
		cout<<clas<<endl;

	}

	//d)5 input majority
	cout<<"5 input majority"<<endl;
	for(string s: answer){
		vector<int> v=s2Vec(s);
		int clas=(majority(s));
		
		cout<< clas<<endl;

	}

	//e) 6 input parity
	cout<<"6 input parity"<<endl;
	n=6;
	while(!answer.empty()){
		answer.pop_back();
	}
	getStrings("",n);
	for(string s: answer){
		vector<int> v=s2Vec(s);
		int clas=(evenParity(s));
		
		cout<< clas<<endl;

	}

}




