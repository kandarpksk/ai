#include <iostream>
#include <vector>
using namespace std;

typedef vector<int> vi;

int dot( const vi& a, const vi& b ){
	int res = 0;
	for( int i = 0; i < a.size(); i++ )
		res += a[i] * b[i];
	return res;
}

void add_to( vi& a, const vi& b ){
	for( int i = 0; i < a.size(); i++ )
		a[i] += b[i];
}

void invert( vi& x ){
	for( int i = 0; i < x.size(); i++ )
		x[i] = -1 * x[i];
}

void print( const vi& x, bool space = false ){
	for( int i = 0; i < x.size(); i++ ){
		if( x[i] >= 0 and space ) cout << " ";
		cout << x[i] << " ";
	}
	cout << "\n";
}

bool present_in( const vector<vi>& list, const vi& x ){
	for( int i = 0; i < list.size(); i++ )
		if( list[i] == x ) return true;
	return false;
}

int main(){

	int n, nrows;
	cout << "Enter number of input lines: ";
	cin >> n;
	nrows = (1 << n);
	
	vi rows[nrows];
	for( int i = 0; i < nrows; i++ ){
		//int c = (outs[i] == 0) ? -1 : 1;
		rows[i].push_back( -1 );
		for( int j = 0, k = i; j < n; j++ ){
			rows[i].push_back( (k & 1) );
			k = k >> 1;
		}
	}

	cout << "Enter the " << nrows << " outputs: ";
	vi outs;
	for( int i = 0, temp; i < nrows; i++ ){
		cin >> temp;
		outs.push_back( temp );
		if( outs[i] == 0 )
			invert( rows[i] );
	}
	
	///*
	cout << endl;
	
	for( int i = 0; i < nrows; i++ )
		print( rows[i], true );
	//*/

	cout << endl;
	
	vi w (n+1, 0);
	vector<vi> seen;
	for( int i = 0; 1; i++ ){
		
		cout << "Step #";
		if( i < 10 ) cout << "0";
		cout << i << ": "; // Step Number
 		print( w, true );
		
		if( present_in( seen, w ) ){
			cout << endl << "Cycle just began again :(\n";
			return(-1);
		}
		else seen.push_back( w );
		
		bool failed_and_fixed = false;
		
		for( int i = 0; i < nrows; i++ ){
			if( dot( w, rows[i] ) <= 0 ){
				add_to( w, rows[i] );
				failed_and_fixed = true;
				break;
			}
		}
		
		if( !failed_and_fixed ){
			cout << endl << "Convergence reached :)\n";
			break;
		}
		
	}

}
