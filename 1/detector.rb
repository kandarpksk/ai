# obtained by running ./digit-classify.sh

weights = [
  [7, 6, 5, -2, 3, 2, 2, 24],
  [-2, -2, 3, -2, -2, 2, -2, 4],
  [6, -2, 5, 3, 2, -2, 2, 17],
  [6, -2, 5, 3, -2, 2, 2, 17],
  [-2, 5, 3, 2, -2, 2, -2, 11],
  [6, 5, -2, 3, -2, 2, 2, 17],
  [7, 6, -2, 5, 3, 2, 2, 24],
  [4, -2, 3, -2, -2, 2, -2, 8],
  [8, 7, 6, 5, 4, 2, 2, 33],
  [7, 6, 5, 3, -2, 2, 2, 24],
]

def dotprd(a1,a2); a1.zip(a2).map{ |x,y| x*y }.reduce(:+); end

input = (0...7).map { gets.to_i }.push(-1)

10.times do |digit|
  if dotprd(input,weights[digit]) > 0 then puts "Detected #{digit}"; break
  elsif digit == 9 then puts "Invalid input" end
end