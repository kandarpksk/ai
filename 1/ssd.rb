  #     0
  # 1       2
  #     3
  # 4       5
  #     6

# ssd = seven segment display
# n = digit
# output = array of on segments
def ssd(n)
  case n
  when 0 then [1,1,1,0,1,1,1]
  when 1 then [0,0,1,0,0,1,0]
  when 2 then [1,0,1,1,1,0,1]
  when 3 then [1,0,1,1,0,1,1]
  when 4 then [0,1,1,1,0,1,0]
  when 5 then [1,1,0,1,0,1,1]
  when 6 then [1,1,0,1,1,1,1]
  when 7 then [1,0,1,0,0,1,0]
  when 8 then [1,1,1,1,1,1,1]
  when 9 then [1,1,1,1,0,1,1]
  end
end

$rev_ssd_arr = (0..9).map { |i| ssd(i) }.transpose

# n = input segments
# output = array of digits with 1 = when it is on, 0 = when it is off
def rev_ssd(n)
  $rev_ssd_arr[n]
end

10.times do |i|
  File.open("training-data/#{i}.txt", "w") do |f|
    f.write("7\n")
    128.times do |feature_vec|
      output = (ssd(i) == feature_vec.to_s(2).rjust(7,'0').split('').map(&:to_i)) ? 1 : 0
      f.write("#{output}\n")
    end
  end
end