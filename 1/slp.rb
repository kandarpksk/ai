# slp = single layer perceptron
# pta = perceptron training algorithm
# run as: ruby slp.rb < {text file with first line = number of inputs n
#                        followed by 2**n lines of 0 or 1}

class Array; def add!(a); self.replace(zip(a).map{ |x,y| x+y }); end; end
def dotprd(a1,a2); a1.zip(a2).map{ |x,y| x*y }.reduce(:+); end
def pta(n, w, verbose=false)
  steps, count, seen, nrows = 0, 0, {}, 1<<n
  outs = (0...nrows).map { gets.to_i }
  rows = (0...nrows).map { |i| i.to_s(2).rjust(n,'0').split('').map(&:to_i).push(-1) }
  nrows.times { |i| outs[i] == 0 ? rows[i].map! { |x| -x } : nil }
  while count < nrows
    if seen[w] then print "Cycle detected! "; return w, steps else seen[w] = true end
    count, steps = 0, steps+1 # reset count, increment num steps
    rows.each { |r| if dotprd(w, r) <= 0 then w.add!(r); break; else count += 1 end }
    if verbose then print "Step #{steps} ", w.inspect, "\n" end
  end
  puts "Steps taken: #{steps}" if verbose
  return verbose ? [w,steps] : w
end
p pta((n=gets.to_i), [0]*(n+1))

# explanation: when count equals nrows we know that every vector matched successfully
# else, at least one vector somewhere had failed