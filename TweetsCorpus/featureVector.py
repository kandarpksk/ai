import re
import sys
import csv

def replaceMultiple(s):
	pat= re.compile(r"(.)\1{1,}", re.DOTALL)
	return pat.sub(r"\1\1",s)

def populateStopWords(fname):
	stopWords=[]
	file=open(fname, 'r')
	line = file.readline()
	while line:
		stopWords.append(line.strip())
		line = file.readline()
	file.close()
	return stopWords

#make the feature vector for a given tweet s
def featureVector(s):
	fv=[]
	words=s.split()
	for w in words:
		w=replaceMultiple(w)
		#also need to remove punctuations
		w=w.strip('"?,.\'!')
		#check if it starts with alphabet

		val=re.search(r"^[a-zA-Z][a-zA-Z0-9]*$",w)
		#Ditch if u are a stopword
		if(w in stopWords or val is None):
			continue
		else:
			fv.append(w.lower())
	return fv

def filterTweet(s):
	s=s.lower()
	#remove URLs
	s=re.sub('((www\.[\s]+)|(https?://[^\s]+))',' ',s)
	#remove @usernames
	s=re.sub('@[^\s]+',' ',s)
	#remove extra whitespaces
	s=re.sub('[\s]+',' ',s)
	# remove # characters for only the hashtag
	s=re.sub(r'#([^\s]+)',r'\1',s)
	# Finally strip off ' and "
	s=s.strip('\'"')
	return s
	

stopWords=populateStopWords('stopwords.txt')

tweets=[]
input=csv.reader(open('twitter_objective', 'rb'), delimiter=',', quotechar='|')

for line in input:
    sentiment = line[0]
    tweet = line[1]
    filteredTweet = filterTweet(tweet)
    fv = featureVector(filteredTweet)
    tweets.append((fv, sentiment));

input=csv.reader(open('twitter_positive', 'rb'), delimiter=',', quotechar='|')

for line in input:
    sentiment = line[0]
    tweet = line[1]
    filteredTweet = filterTweet(tweet)
    fv = featureVector(filteredTweet)
    tweets.append((fv, sentiment));
input=csv.reader(open('twitter_negative', 'rb'), delimiter=',', quotechar='|')

for line in input:
    sentiment = line[0]
    tweet = line[1]
    filteredTweet = filterTweet(tweet)
    fv = featureVector(filteredTweet)
    tweets.append((fv, sentiment));
impWords=set()
for i in tweets:
	for j in i[0]:
		impWords.add(j)
impWords =  (sorted(list(impWords)))
#print len(impWords)
def makeFV(sTweet):
	s=sTweet.lower()
	s=set(sTweet.split())
	l=[1 if x in s else 0 for x in impWords]
	return l
strg='Fuck this economy. I hate aig and their non loan given asses.'

def makeFeatureVec(strg):
	return makeFV(' '.join(featureVector(filterTweet(strg))))
print "HI"
#print makeFeatureVec(strg)

