from math import e as ee
from random import random as rand
import re
import csv
import sys
import random
import string
diff = lambda l1,l2: filter(lambda x: x not in l2, l1)
###########################Feature Vector Part##########################
def replaceMultiple(s):
	pat= re.compile(r"(.)\1{1,}", re.DOTALL)
	return pat.sub(r"\1\1",s)

def populateStopWords(fname):
	stopWords=[]
	file=open(fname, 'r')
	line = file.readline()
	while line:
		stopWords.append(line.strip())
		line = file.readline()
	file.close()
	return stopWords

#make the feature vector for a given tweet s
def featureVector(s):
	fv=[]
	words=s.split()
	for w in words:
		w=replaceMultiple(w)
		#also need to remove punctuations
		w=w.strip('"?,.\'!')
		#check if it starts with alphabet

		val=re.search(r"^[a-zA-Z][a-zA-Z0-9]*$",w)
		#Ditch if u are a stopword
		if(w in stopWords or val is None):
			continue
		else:
			fv.append(w.lower())
	return fv

def filterTweet(s):
	s=s.lower()
	#remove URLs
	s=re.sub('((www\.[\s]+)|(https?://[^\s]+))',' ',s)
	#remove @usernames
	s=re.sub('@[^\s]+',' ',s)
	#remove extra whitespaces
	s=re.sub('[\s]+',' ',s)
	# remove # characters for only the hashtag
	s=re.sub(r'#([^\s]+)',r'\1',s)
	# Finally strip off ' and "
	s=s.strip('\'"')
	return s
	

stopWords=populateStopWords('stopwords.txt')

tweets=[]
input=csv.reader(open('twitter_objective', 'rb'), delimiter=',', quotechar='|')

for line in input:
    sentiment = line[0]
    tweet = line[1]
    filteredTweet = filterTweet(tweet)
    fv = featureVector(filteredTweet)
    tweets.append((fv, sentiment));

input=csv.reader(open('twitter_positive', 'rb'), delimiter=',', quotechar='|')

for line in input:
    sentiment = line[0]
    tweet = line[1]
    filteredTweet = filterTweet(tweet)
    fv = featureVector(filteredTweet)
    tweets.append((fv, sentiment));
input=csv.reader(open('twitter_negative', 'rb'), delimiter=',', quotechar='|')

for line in input:
    sentiment = line[0]
    tweet = line[1]
    filteredTweet = filterTweet(tweet)
    fv = featureVector(filteredTweet)
    tweets.append((fv, sentiment));
impWords=set()
for i in tweets:
	for j in i[0]:
		impWords.add(j)
impWords =  (sorted(list(impWords)))
#print len(impWords)
def makeFV(sTweet):
	s=sTweet.lower()
	s=set(sTweet.split())
	l=[1 if x in s else 0 for x in impWords]
	return l

def makeFeatureVec(strg):
	return makeFV(' '.join(featureVector(filterTweet(strg))))


###########################Neural Nets Part##########################
def sigmoid(n): return 1.0/(1 + ee**(-n))

def activate(a, b): return 1 if a > b else 0

momentum = 0.3
learning_rate = 0.9999
class Node:
	def __init__(self):
		self.recentOutput = None
		self.recentInput = None
		self.error = None
		self.inEdges = []
		self.outEdges = []


	def eval(self, inputVector):
		if self.recentOutput:
			return self.recentOutput
		self.recentInput = []
		dotprd = 0
		for edge in self.inEdges:
			ip = edge.src.eval(inputVector)
			self.recentInput.append(ip)
			dotprd += edge.weight * ip
		self.recentOutput = sigmoid(dotprd)
		return self.recentOutput

	def errorCalc(self, expected):
		if self.error:
			return self.error
		if self.outEdges == []:
			self.error = expected - self.recentOutput
		else:
			self.error = sum(map(lambda e: e.weight*e.dest.errorCalc(expected), self.outEdges))
		return self.error

	def updateWeights(self, lrate):
		if self.error and self.recentOutput and self.recentInput:
			for i,e in enumerate(self.inEdges):
				e.deltawt = lrate * self.recentOutput * \
							(1-self.recentOutput) * self.error * \
							self.recentInput[i] + momentum*e.deltawt #momentum seems to be 0.9 typically, (source : wikibooks)
				e.weight += e.deltawt
			for e in self.outEdges:
				e.dest.updateWeights(lrate)
			self.error = None
			self.recentInput = None
			self.recentOutput = None

	def remComputations(self):
		if self.recentOutput:
			self.recentOutput = None
			for e in self.inEdges:
				e.src.remComputations()

class InputNode(Node):
	def __init__(self, index):
		self.recentOutput = None
		self.recentInput = None
		self.error = None
		self.inEdges = []
		self.outEdges = []
		self.index = index

	def eval(self, inputVector):
		self.recentOutput = inputVector[self.index]
		return self.recentOutput

	def updateWeights(self, lrate):
		for e in self.outEdges:
			e.dest.updateWeights(lrate)

	def errorCalc(self, expected):
		for e in self.outEdges:
			e.dest.errorCalc(expected)

	def remComputations(self):
		self.recentOutput = None

class BiasNode(InputNode):
	def __init__(self):
		self.recentOutput = None
		self.recentInput = None
		self.error = None
		self.inEdges = []
		self.outEdges = []

	def eval(self, inputVector):
		return 1

class Edge:
	def __init__(self, src, dest):
		self.weight = rand()
		#print self.weight
		self.src = src
		self.dest = dest
		self.src.outEdges.append(self)
		self.dest.inEdges.append(self)
		self.deltawt = 0.0

class NeuralNet:
	def __init__(self):
		self.inputNodes = []
		self.outputNode = None

	def eval(self, inputVector):
		self.outputNode.remComputations()
		return self.outputNode.eval(inputVector)

	def propagateError(self, expected):
		for n in self.inputNodes:
			n.errorCalc(expected)

	def updateWeights(self, lrate):
		for n in self.inputNodes:
			n.updateWeights(lrate)

	def trainItAway(self, truthtable, lrate):
		net_error = 10
		num_iter = 0
		while abs(net_error) > 1.0:
			errors = []
			for i,entry in enumerate(truthtable):
				self.eval(entry[0])
				self.propagateError(entry[1])
				errors.append(self.outputNode.errorCalc(entry[1]))
				self.updateWeights(lrate)
			net_error = sum([x**2 for x in errors])
			if num_iter % 20 == 0:
				print "Iteration:", num_iter, "Error:", net_error
			num_iter += 1
		print "Error too small, breaking out!"

n = len(impWords)

#nrows = 1 << n

neuralNetwork = NeuralNet()
inputNodes = [InputNode(i-1) for i in xrange(1,n+1)]
hiddenNodes = [Node() for i in xrange(1,1)]
outputNode = Node()
#for h in hiddenNodes:
#	h.inEdges.append(Edge(BiasNode(),h))

Edge(BiasNode(), outputNode)

for i in inputNodes:
	#for j in hiddenNodes:
		Edge(i,outputNode)

#for j in hiddenNodes:
#	Edge(j, outputNode)

neuralNetwork.outputNode = outputNode
neuralNetwork.inputNodes += inputNodes
inputs=[]
outputs=[]
'''input=csv.reader(open('twitter_negative', 'rb'), delimiter=',', quotechar='|')

for line in input:
    sentiment = line[0]
    tweet = line[1]
    inputs.append(makeFeatureVec(tweet))
    outputs.append(0)

input=csv.reader(open('twitter_objective', 'rb'), delimiter=',', quotechar='|')

for line in input:
    sentiment = line[0]
    tweet = line[1]
    inputs.append(makeFeatureVec(tweet))
    outputs.append(1)

input=csv.reader(open('twitter_positive', 'rb'), delimiter=',', quotechar='|')

for line in input:
    sentiment = line[0]
    tweet = line[1]
    inputs.append(makeFeatureVec(tweet))
    outputs.append(0)'''
input=csv.reader(open('tweets.txt', 'rb'), delimiter=',', quotechar='|')
print "sdvsdc"
storeTweets=[]
for line in input:
	storeTweets.append(line)
print "sdvsdc"

i=string.atoi(sys.argv[1])
random.shuffle(storeTweets)
testingSet=storeTweets[i*len(storeTweets)/6 : 1+(i+1)*len(storeTweets)/6]
trainingSet=diff(storeTweets,testingSet)
print "sdvsdc"

for l in trainingSet:
	print "1"
	sentiment = l[0]
	tweet = l[1]
	inputs.append(makeFeatureVec(tweet))
	outputs.append(1 if sentiment=='positive' else 0)

print "Starting TT"
truthtable = [[x,y] for x,y in zip(inputs,outputs)]
print "Starting Training"
print inputs[0]
erroneous=0
neuralNetwork.trainItAway(truthtable, learning_rate)
for l in testingSet:
	sentiment=l[0]
	tweet=l[1]
	fv1=makeFeatureVec(tweet)
	act=activate(neuralNetwork.eval(fv1),0.5)
	print tweet,act,(1 if sentiment=='positive' else 0),sentiment
	if (act != (1 if sentiment=='positive' else 0)):
		erroneous += 1 


print erroneous*100.0/len(testingSet)
#nnOutputs = [activate(neuralNetwork.eval(row), 0.5) for row in inputs]

print "Expected: ", outputs
#print "Result:   ", nnOutputs
#if nnOutputs == outputs:
#	print "Perfect match"

#nnIP = [[x,y] for x,y in zip(inputs, nnOutputs)]
print "###########################"
while 1:
	newTweet=raw_input()
	print neuralNetwork.eval(makeFeatureVec(newTweet))
#print [activate(neuralNetwork.eval(row), 0.5) for row in testingSet]
