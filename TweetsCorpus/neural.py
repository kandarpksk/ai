from math import e as ee
from random import random as rand
import sys

def sigmoid(n): return 1.0/(1 + ee**(-n))

def activate(a, b): return 1 if a > b else 0

momentum = 0.3
learning_rate = 0.9999
class Node:
	def __init__(self):
		self.recentOutput = None
		self.recentInput = None
		self.error = None
		self.inEdges = []
		self.outEdges = []


	def eval(self, inputVector):
		if self.recentOutput:
			return self.recentOutput
		self.recentInput = []
		dotprd = 0
		for edge in self.inEdges:
			ip = edge.src.eval(inputVector)
			self.recentInput.append(ip)
			dotprd += edge.weight * ip
		self.recentOutput = sigmoid(dotprd)
		return self.recentOutput

	def errorCalc(self, expected):
		if self.error:
			return self.error
		if self.outEdges == []:
			self.error = expected - self.recentOutput
		else:
			self.error = sum(map(lambda e: e.weight*e.dest.errorCalc(expected), self.outEdges))
		return self.error

	def updateWeights(self, lrate):
		if self.error and self.recentOutput and self.recentInput:
			for i,e in enumerate(self.inEdges):
				e.deltawt = lrate * self.recentOutput * \
							(1-self.recentOutput) * self.error * \
							self.recentInput[i] + momentum*e.deltawt #momentum seems to be 0.9 typically, (source : wikibooks)
				e.weight += e.deltawt
			for e in self.outEdges:
				e.dest.updateWeights(lrate)
			self.error = None
			self.recentInput = None
			self.recentOutput = None

	def remComputations(self):
		if self.recentOutput:
			self.recentOutput = None
			for e in self.inEdges:
				e.src.remComputations()

class InputNode(Node):
	def __init__(self, index):
		self.recentOutput = None
		self.recentInput = None
		self.error = None
		self.inEdges = []
		self.outEdges = []
		self.index = index

	def eval(self, inputVector):
		self.recentOutput = inputVector[self.index]
		return self.recentOutput

	def updateWeights(self, lrate):
		for e in self.outEdges:
			e.dest.updateWeights(lrate)

	def errorCalc(self, expected):
		for e in self.outEdges:
			e.dest.errorCalc(expected)

	def remComputations(self):
		self.recentOutput = None

class BiasNode(InputNode):
	def __init__(self):
		self.recentOutput = None
		self.recentInput = None
		self.error = None
		self.inEdges = []
		self.outEdges = []

	def eval(self, inputVector):
		return 1

class Edge:
	def __init__(self, src, dest):
		self.weight = rand()
		print self.weight
		self.src = src
		self.dest = dest
		self.src.outEdges.append(self)
		self.dest.inEdges.append(self)
		self.deltawt = 0.0

class NeuralNet:
	def __init__(self):
		self.inputNodes = []
		self.outputNode = None

	def eval(self, inputVector):
		self.outputNode.remComputations()
		return self.outputNode.eval(inputVector)

	def propagateError(self, expected):
		for n in self.inputNodes:
			n.errorCalc(expected)

	def updateWeights(self, lrate):
		for n in self.inputNodes:
			n.updateWeights(lrate)

	def trainItAway(self, truthtable, lrate):
		net_error = 10
		num_iter = 0
		while abs(net_error) > 0.5:
			errors = []
			for i,entry in enumerate(truthtable):
				self.eval(entry[0])
				self.propagateError(entry[1])
				errors.append(self.outputNode.errorCalc(entry[1]))
				self.updateWeights(lrate)
			net_error = sum([x**2 for x in errors])
			if num_iter % 5000 == 0:
				print "Iteration:", num_iter, "Error:", net_error
			num_iter += 1
		print "Error too small, breaking out!"

n = int(input())
nrows = 1 << n

neuralNetwork = NeuralNet()
inputNodes = [InputNode(i-1) for i in xrange(1,n+1)]
hiddenNodes = [Node() for i in xrange(1,n+1)]
outputNode = Node()
for h in hiddenNodes:
	h.inEdges.append(Edge(BiasNode(),h))

Edge(BiasNode(), outputNode)

for i in inputNodes:
	for j in hiddenNodes:
		Edge(i,j)

for j in hiddenNodes:
	Edge(j, outputNode)

neuralNetwork.outputNode = outputNode
neuralNetwork.inputNodes += inputNodes

inputs = [[int(x) for x in list(("{0:0"+str(n)+"b}").format(i))] for i in xrange(nrows)]
outputs = [int(input()) for i in xrange(nrows)]

truthtable = [[x,y] for x,y in zip(inputs,outputs)]

neuralNetwork.trainItAway(truthtable, learning_rate)

nnOutputs = [activate(neuralNetwork.eval(row), 0.5) for row in inputs]

print "Expected: ", outputs
print "Result:   ", nnOutputs
if nnOutputs == outputs:
	print "Perfect match"

nnIP = [[x,y] for x,y in zip(inputs, nnOutputs)]